package com.example.hw11;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView car1 = findViewById(R.id.imageViewCar1);
        ImageView car2 = findViewById(R.id.imageViewCar2);
        ImageView car3 = findViewById(R.id.imageViewCar3);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.startRace) {
                    try {
                        int response1 = Math.abs(new StartRace(car1).execute(0).get());
                        int response2 = Math.abs(new StartRace(car2).execute(0).get());
                        int response3 = Math.abs(new StartRace(car3).execute(0).get());

                        int max = Math.max(Math.max(response1, response2), response3);

                        if (max == response1) {
                            Toast.makeText(MainActivity.this,
                                    "Car 1 Win!",
                                    Toast.LENGTH_LONG).show();
                        } else if (max == response2) {
                            Toast.makeText(MainActivity.this,
                                    "Car 2 Win!",
                                    Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(MainActivity.this,
                                    "Car 3 Win!",
                                    Toast.LENGTH_LONG).show();
                        }
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    return true;
                }
                return false;
            }
        });

    }
}