package com.example.hw11;

import android.os.AsyncTask;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

import java.util.Random;

public  class  StartRace extends AsyncTask<Integer, Void,Integer > {

    Animation animation = null;
    ImageView car;

    public StartRace(ImageView car) {
        this.car=car;
    }

    protected int getRandomStep(int min, int max) {
        Random random = new Random();
        return random.nextInt(max - min) + min;

    }

    @Override
    protected Integer doInBackground(Integer... voids) {
        int step = getRandomStep(-1000, -500);
        return step;
    }


    @Override
    protected void onPostExecute(Integer integer) {
        super.onPostExecute(integer);
        animation = new TranslateAnimation(0,integer,0,0);
        AnimationSet set = new AnimationSet(true);
        animation.setDuration(3000);
        animation.setInterpolator(new AccelerateInterpolator());
        //animation.setRepeatMode(2);
        //animation.setRepeatCount(animation.getRepeatMode());
        animation.setFillAfter(true);
        set.addAnimation(animation);
        car.startAnimation(animation);
    }

}

